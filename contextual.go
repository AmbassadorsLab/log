package log

import (
	opentracing "github.com/opentracing/opentracing-go"
	opentracingext "github.com/opentracing/opentracing-go/ext"
	opentracinglog "github.com/opentracing/opentracing-go/log"
)

type contextualLogger struct {
	logger Logger
	span   opentracing.Span
}

func (cl contextualLogger) Log(keyvals ...interface{}) error {
	if err := cl.logger.Log(keyvals...); err != nil {
		return err
	}

	if cl.span != nil {
		fields, err := opentracinglog.InterleavedKVToFields(keyvals...)
		if err != nil {
			return err
		}

		// check if it needs to be logged or tagged
		var logFields []opentracinglog.Field
		for _, field := range fields {
			switch field.Key() {
			case "event":
				if field.Value() == "error" {
					opentracingext.Error.Set(cl.span, true)
				}

				logFields = append(logFields, field)

			case "message":
				logFields = append(logFields, field)

			default:
				cl.span.SetTag(field.Key(), field.Value())
			}
		}

		if len(logFields) > 0 {
			cl.span.LogFields(logFields...)
		}
	}

	return nil
}
