package log

import (
	"context"
	"os"

	"github.com/go-kit/kit/log"
	opentracing "github.com/opentracing/opentracing-go"
)

type Logger interface {
	Log(keyvals ...interface{}) error
}

func With(logger Logger, keyvals ...interface{}) Logger {
	return log.With(logger, keyvals...)
}

func For(logger Logger, ctx context.Context) Logger {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		return contextualLogger{
			logger: logger,
			span:   span,
		}
	}

	return logger
}

func ForOperation(logger Logger, ctx context.Context, operationName string, opts ...opentracing.StartSpanOption) (Logger, opentracing.Span, context.Context) {
	span, ctx := opentracing.StartSpanFromContext(ctx, operationName, opts...)

	logger = For(logger, ctx)

	return logger, span, ctx
}

func New() Logger {
	logger := log.NewLogfmtLogger(log.NewSyncWriter(os.Stdout))
	logger = With(logger, "timestamp", log.DefaultTimestampUTC)

	return logger
}

func NewDefault(serviceName string) Logger {
	logger := New()

	logger = With(logger, "service", serviceName)

	return logger
}

type nopLogger struct{}

func NewNopLogger() Logger { return nopLogger{} }

func (nopLogger) Log(...interface{}) error { return nil }
