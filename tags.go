package log

import (
	opentracing "github.com/opentracing/opentracing-go"
	opentracingext "github.com/opentracing/opentracing-go/ext"
)

type componentOption struct {
	component string
}

func (c componentOption) Apply(o *opentracing.StartSpanOptions) {
	component := "undefined"
	if c.component != "" {
		component = c.component
	}

	opentracing.Tag{Key: string(opentracingext.Component), Value: component}.Apply(o)
}

func ComponentOption(component string) opentracing.StartSpanOption {
	return componentOption{component}
}

type peerAddressOption struct {
	peerAddress string
}

func (po peerAddressOption) Apply(o *opentracing.StartSpanOptions) {
	peerAddress := "undefined"
	if po.peerAddress != "" {
		peerAddress = po.peerAddress
	}

	opentracing.Tag{Key: string(opentracingext.PeerAddress), Value: peerAddress}.Apply(o)
}

func PeerAddressOption(peerAddress string) opentracing.StartSpanOption {
	return peerAddressOption{peerAddress}
}
